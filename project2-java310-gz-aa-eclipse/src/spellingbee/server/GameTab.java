package spellingbee.server;
import spellingbee.network.*;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class GameTab  extends Tab {
	private Client client;
	
	// store all letters, center letter received from Server. Store all letters without the center character Letter as String to use
	private String allLetters;
	private char centerLetter;
	private String allLettersNoCenterLetter;
	
	// all 7 letter buttons to return for the Applications
	private Button firstLetterB;
	private Button secondLetterB;
	private Button thirdLetterB;
	private Button fourthCenterLetterB;
	private Button fifthLetterB;
	private Button sixthLetterB;
	private Button seventhLetterB;
	private Button submitButton;
	private Button clearB;
	private Button deleteB;
	
	private TextField attemptField;
	private TextField messageField;
	private TextField scoreField;
	
	private String attempt;

	public GameTab( Client client) {
		super("Game");
		this.client=client;
		
		this.allLetters = this.client.sendAndWaitMessage("getAllLetters");
		this.centerLetter = this.client.sendAndWaitMessage("getCenterLetter").charAt(0);
		this.allLettersNoCenterLetter = this.allLetters.replace(Character.toString(this.centerLetter), "");
		
		this.firstLetterB = new Button(Character.toString(this.allLettersNoCenterLetter.charAt(0)));
		this.secondLetterB = new Button(Character.toString(this.allLettersNoCenterLetter.charAt(1)));
		this.thirdLetterB = new Button(Character.toString(this.allLettersNoCenterLetter.charAt(2)));
		this.fourthCenterLetterB = new Button(Character.toString(this.centerLetter));
		this.fifthLetterB = new Button(Character.toString(this.allLettersNoCenterLetter.charAt(3)));
		this.sixthLetterB = new Button(Character.toString(this.allLettersNoCenterLetter.charAt(4)));
		this.seventhLetterB = new Button(Character.toString(this.allLettersNoCenterLetter.charAt(5)));

		this.attemptField = new TextField("");		
		this.submitButton = new Button("submit");
		this.messageField = new TextField("");
		this.scoreField = new TextField("");			
		this.messageField.setText(this.client.sendAndWaitMessage("message;"+this.attempt));
		this.scoreField.setText(this.client.sendAndWaitMessage("getScore"));
		
		this.clearB = new Button("clear");
		this.deleteB = new Button("delete");
	
	}

	public Button getFirstLetterB() {
		return firstLetterB;
	}

	public void setFirstLetterB(Button firstLetterB) {
		this.firstLetterB = firstLetterB;
	}

	public Button getSecondLetterB() {
		return secondLetterB;
	}

	public void setSecondLetterB(Button secondLetterB) {
		this.secondLetterB = secondLetterB;
	}

	public Button getThirdLetterB() {
		return thirdLetterB;
	}

	public void setThirdLetterB(Button thirdLetterB) {
		this.thirdLetterB = thirdLetterB;
	}

	public Button getFourthCenterLetterB() {
		return fourthCenterLetterB;
	}

	public void setFourthCenterLetterB(Button fourthCenterLetterB) {
		this.fourthCenterLetterB = fourthCenterLetterB;
	}

	public Button getFifthLetterB() {
		return fifthLetterB;
	}

	public void setFifthLetterB(Button fifthLetterB) {
		this.fifthLetterB = fifthLetterB;
	}

	public Button getSixthLetterB() {
		return sixthLetterB;
	}

	public void setSixthLetterB(Button sixthLetterB) {
		this.sixthLetterB = sixthLetterB;
	}

	public Button getSeventhLetterB() {
		return seventhLetterB;
	}

	public void setSeventhLetterB(Button seventhLetterB) {
		this.seventhLetterB = seventhLetterB;
	}
	
	public TextField getAttemptField() {
		return attemptField;
	}

	public void setAttemptField(TextField attemptField) {
		this.attemptField = attemptField;
	}
	
	public Button getSubmitButton() {
		return submitButton;
	}

	public void setSubmitButton(Button submitButton) {
		this.submitButton = submitButton;
	}

	public TextField getMessageField() {
		return messageField;
	}

	public void setMessageField(TextField messageField) {
		this.messageField = messageField;
	}

	public TextField getScoreField() {
		return scoreField;
	}

	public void setScoreField(TextField scoreField) {
		this.scoreField = scoreField;
	}
	
	public String getAllLetters() {
		return allLetters;
	}

	public void setAllLetters(String allLetters) {
		this.allLetters = allLetters;
	}

	public char getCenterLetter() {
		return centerLetter;
	}

	public void setCenterLetter(char centerLetter) {
		this.centerLetter = centerLetter;
	}

	public Button getClearB() {
		return clearB;
	}

	public void setClearB(Button clearB) {
		this.clearB = clearB;
	}

	public Button getDeleteB() {
		return deleteB;
	}

	public void setDeleteB(Button deleteB) {
		this.deleteB = deleteB;
	}

	public String getAttempt() {
		return attempt;
	}

	public void setAttempt(String attempt) {
		this.attempt = attempt;
	}

	public String getAllLettersNoCenterLetter() {
		return allLettersNoCenterLetter;
	}

	public void setAllLettersNoCenterLetter(String allLettersNoCenterLetter) {
		this.allLettersNoCenterLetter = allLettersNoCenterLetter;
	}
	
}
	
	

