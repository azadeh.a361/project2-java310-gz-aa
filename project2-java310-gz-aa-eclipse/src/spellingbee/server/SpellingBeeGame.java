package spellingbee.server;
import java.nio.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.*;
import java.util.*;

public class SpellingBeeGame implements ISpellingBeeGame {
	
	

	private String allLetters;
	private char centralletter;
	private int currentScore;
	private HashSet<String> foundWords=new HashSet<String>();
	private ArrayList<String> possibleWords=new ArrayList<String>();
	
	public SpellingBeeGame() {
		try {
			this. foundWords=new HashSet<String>();
			this.currentScore=currentScore(this.foundWords);
			this.allLetters=getRandomLetterSet(".\\.\\.\\datafiles\\letterCombinations.txt" );
			this.centralletter=getRandomLetter(this.allLetters);
			this.possibleWords=createWordsFromFile(".\\.\\.\\datafiles\\english.txt");}
			catch(IOException e) {};
		
	}
	
	public SpellingBeeGame (String allLetters) { 
		try {
		this.allLetters=allLetters;
		this.currentScore=currentScore(foundWords);
		this.centralletter=getRandomLetter(this.allLetters);
		
		this.possibleWords=createWordsFromFile(".\\.\\.\\datafiles\\english.txt");}
		catch(IOException e) {};
	}
	
	/*
	 * this method gets the path for dictionary and returns
	 * an arraylist of the words all in lower case 
	 * @param String path 
	 * @return an arraylist of the dictionary */
	
	 public static ArrayList <String> createWordsFromFile(String path) throws IOException { 
		 Path p = Paths.get(path);
		 List<String> english = Files.readAllLines(p); 
		for (String x : english) {
			x.toLowerCase();
		}
		 return (ArrayList<String>)english;
		 
		}
	 /* this method gets the path for collection of letters and uses the
	  * createWordsFromFile method to change them to an arraylist and then gets a set of
	  * letters by random 
	 * @param String path 
	 * @return a string of letters */
	
	
	public static String getRandomLetterSet(String path) throws IOException{
		Random x=new Random();
		ArrayList<String>letters=createWordsFromFile( path);
		int i=x.nextInt(letters.size());
		String allLetters=letters.get(i);
		return allLetters;
	
}
	/* this method gets the String of letters and gets a 
	  * letter by random 
	 * @param String letters 
	 * @return a Char  */
	public static char getRandomLetter(String letter) {
		Random x=new Random();
		
		int i=x.nextInt(7);
		char centerletter=letter.charAt(i);
		return centerletter;}	
	
	/* this method gets the String of user Attempt and gives out a message based on the
	 * game criteria 
	 * @param String attempt 
	 * @return a string of message */
	
	public String getMessage(String attempt) {
		String message="";
		if ( this.foundWords.contains(attempt)){
			message="You already entered this word";
		}
		else if (attempt.length()<4) {
			 message ="The word is too short";}
		else if (attempt.contains("[^allLetters.charAt(0)-allLetters.charAt(1)-allLetters.charAt(2)-allLetters.charAt(3)-allLetters.charAt(4)-allLetters.charAt(5)-allLetters.charAt(6)]"))
			message="The word contains invalid letters";
			else if(! (attempt.contains(""+this.centralletter))) {
			 message="The Central word is not included";
		}
			else if (this.possibleWords.contains(attempt)) {
				 message="great!";}
				else
					 message="It is Not a Word!";
			return message;
		}
	
	/* this method gets the String of user Attempt  and uses the getMessage method
	 * if the word is a valid one it counts the score and gives out a score based on the
	 * game criteria also if the word score is not 0 it will add it to foundword set 
	 * @param String attempt 
	 * @return a int score */
	
	public int getPointsForWord(String attempt) {
		int score=0;
		
		if (this.getMessage(attempt).equals("great!")) {
			if (attempt.length()==4) {
				score=1;
		}else if (attempt.length()>4 && attempt.length()<=6) {
			score=attempt.length();}
			else if (attempt.length()>=7) {
				if (attempt.contains(String.valueOf(this.allLetters.charAt(0)))&&
					attempt.contains(String.valueOf(this.allLetters.charAt(1)))&&
						attempt.contains(String.valueOf(this.allLetters.charAt(2)))&&
						attempt.contains(String.valueOf(this.allLetters.charAt(3)))&&
						attempt.contains(String.valueOf(this.allLetters.charAt(4)))&&
						attempt.contains(String.valueOf(this.allLetters.charAt(5)))&&
						attempt.contains(String.valueOf(this.allLetters.charAt(6)))
						)
					score=attempt.length()+7;
					else
						score=attempt.length();
				}
			}
		else 
			{score=0;}
		if (score!=0) {
			this.foundWords.add(attempt);
		}
		return score;
}
	
	/* this method finds all possible words with set of letters and then uses getPointsForWord
	 * to count the the overall score then puts the score into a bracket by different dividend
	 * @return a int[] bracket */
	public int[] getBrackets() {
		ArrayList<String>words=new ArrayList<String>();
		for (int j=0;j<this.possibleWords.size();j++) {
			
		int i=0;
for( i=0;i<this.possibleWords.get(j).length();i++) {
			if(! (this.allLetters.contains(String.valueOf(this.possibleWords.get(j).charAt(i)))))
	
			
				break;
			}

if( i==this.possibleWords.get(j).length()-1)
words.add(this.possibleWords.get(j));
		
	
}
		int sum=0;
		for(String x : words){
		int score=getPointsForWord(x);
		sum=sum+score;}
			
		
		int[]bracket= {25*sum/100,50*sum/100,75*sum/100,90*sum/100,sum};
	
		return bracket;
	}
	
	/* this method gets the set of all words found by user and then using getPointsForWord 
	 * method calculates the current score 
	 * @param Hashset of found words
	 * return int score
	 */
	public int currentScore(HashSet<String> words) {
		 int currentScore=0;
		for ( String word : words) {
		int point=getPointsForWord(word);
		 currentScore= currentScore+point;
		 
	}return  currentScore;
	}
	
	public int getCurrentScore() {
		return currentScore;}
	
	public String getAllLetters() {
		return this.allLetters;
	}
	public char getCenterLetter() {
		return this.centralletter;
	}
	public int getScore() {
		return this.currentScore;}
	}
	
	