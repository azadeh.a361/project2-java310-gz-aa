package spellingbee.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class SimpleSpellingBeeGame implements ISpellingBeeGame {
	private String allLetters;
	private char centerLetter;
	private int score;
	private int pointsForWord;
	private String message;
	private int[] brackets = {50, 100, 150, 180, 200};
	
	public SimpleSpellingBeeGame() {
		this.allLetters = "animels";
		this.centerLetter = 'm';
		this.score = 20;
		this.pointsForWord = 5;
		this.message = "Good";
	}
	// attempt for the word sent to Server; number of points for the word
	// hard coded as 4 for testing
	public int getPointsForWord(String attempt) {
		return this.pointsForWord; 
	};
	
	// attempt for the word sent to Server; message from Server for checking word validity and status
	// hard coded as "Good" for testing
	public String getMessage(String attempt) {
		return this.message;
	};
	
	// return all 7 letters as a String from the spelling bee object
	// hard coded as "animels" for testing
	public String getAllLetters() {
		return this.allLetters;
	};
	
	// return center letter as char which is required for every word
	// hard coded as "m" for testing
	public char getCenterLetter() {
		return this.centerLetter;
	};
	
	// return user's current score as int
	// hard coded as 20 for testing
	public int getScore() {
		return this.score;
	};
	
	// return int[] for point categories
	// hard coded as {50, 100, 150, 180, 200} for testing
	public int[] getBrackets() {
		return this.brackets;
	}
}
