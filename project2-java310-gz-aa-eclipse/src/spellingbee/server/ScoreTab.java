package spellingbee.server;
import spellingbee.network.*;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class ScoreTab  extends Tab{
	private Client client;
	
	private Text queenBee = new Text("Queen Bee");
	private Text genious= new Text("Genius");
	private Text amazing = new Text("Amazing");
	private Text good = new Text("Good");
	private Text started = new Text("Getting Started");
	private Text currentScore= new Text("Current Score");
	private TextField queenBeeVal=new TextField();
	private TextField geniousVal=new TextField() ;
	private TextField amazingVal =new TextField();
	private TextField goodVal=new TextField() ;
	private TextField startedVal=new TextField() ;
	private TextField  currentScoreVal=new TextField();
	private TextField scoreField=new TextField();

	
	public ScoreTab( Client client) {
		super("Score");
	this.client=client;
	this.queenBee = new Text("Queen Bee");
	this.genious= new Text("Genius");
	this. amazing = new Text("Amazing");
	this.good = new Text("Good");
	this.started = new Text("Getting Started");
	this. currentScore= new Text("Current Score");
	this.queenBeeVal=new TextField("");
	this. geniousVal=new TextField("") ;
	this. amazingVal =new TextField("");
	this. goodVal=new TextField("") ;
	this. startedVal=new TextField("") ;
	this.  currentScoreVal=new TextField("");
	this.scoreField=new TextField("");
		this.queenBeeVal.setText(this.client.sendAndWaitMessage("QueenBee"));
		this.geniousVal.setText(this.client.sendAndWaitMessage("Genious"));
		this.amazingVal.setText(this.client.sendAndWaitMessage("Amazing"));
		 this.goodVal.setText(this.client.sendAndWaitMessage("Good"));
		this.startedVal.setText(this.client.sendAndWaitMessage("GettingStarted"));
		/*this.currentScoreVal.setText(this.client.sendAndWaitMessage("CurrentScore"));*/
	
	}
	
	public void refresh() {
		int qbval=Integer.parseInt(this.queenBeeVal.getText());
		int gval=Integer.parseInt(this.geniousVal.getText());
		int aval=Integer.parseInt(this.amazingVal.getText());
		int gdval=Integer.parseInt(this.goodVal.getText());
		int sval=Integer.parseInt(this.startedVal.getText());
		String score=this.client.sendAndWaitMessage("getScore");
		this. currentScoreVal.setText(score);
		int parsedScore=Integer.parseInt(score);
		if (parsedScore<=sval)
		{this.queenBee.setFill(Color.GREY);
		this.genious.setFill(Color.GREY);
		this.amazing.setFill(Color.GREY);
		this.good.setFill(Color.GREY);
		this.started.setFill(Color.GREY);
		}
		else if (parsedScore>=sval && parsedScore<=gdval) {
			queenBee.setFill(Color.GREY);
			genious.setFill(Color.GREY);
			amazing.setFill(Color.GREY);
			good.setFill(Color.GREY);
		}else if (parsedScore>=gdval && parsedScore<=aval) {
			queenBee.setFill(Color.GREY);
			genious.setFill(Color.GREY);
			amazing.setFill(Color.GREY);
		}else if (parsedScore>=aval && parsedScore<=gval) {
			queenBee.setFill(Color.GREY);
			genious.setFill(Color.GREY);
			
		}else if(parsedScore>=gval && parsedScore<=qbval) {
			queenBee.setFill(Color.GREY);
		}
		else
		{this.queenBee.setFill(Color.BLACK);
		this.genious.setFill(Color.BLACK);
		this.amazing.setFill(Color.BLACK);
		this.good.setFill(Color.BLACK);
		this.started.setFill(Color.BLACK);
		}
			
	}

	

	
	

	public Text getQueenBee() {
		return queenBee;
	}

	

	public Text getGenious() {
		return genious;
	}

	

	public Text getAmazing() {
		return amazing;
	}

	
	public Text getGood() {
		return good;
	}

	

	public Text getStarted() {
		return started;
	}

	public void setStarted(Text started) {
		this.started = started;
	}

	public Text getCurrentScore() {
		return currentScore;
	}

	

	public TextField getQueenBeeVal() {
		return queenBeeVal;
	}

	

	public TextField getGeniousVal() {
		return geniousVal;
	}

	

	public TextField getAmazingVal() {
		return amazingVal;
	}

	

	public TextField getGoodVal() {
		return goodVal;
	}

	

	public TextField getStartedVal() {
		return startedVal;
	}

	

	public TextField getCurrentScoreVal() {
		return currentScoreVal;
	}
	public TextField getScoreField() {
		return scoreField;
	}
	

	
	}
	
	
