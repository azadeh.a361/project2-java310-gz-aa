package spellingbee.client;
import spellingbee.network.Server;
import spellingbee.server.*;
import spellingbee.network.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javafx.beans.value.ChangeListener;

 
import javax.swing.event.*;
import javafx.application.*;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class SpellingBeeClient  extends Application{

	private Client client=new  Client() ;
		
	public void start(Stage stage) { 
		TabPane gameAndScore = new TabPane();
		Group root = new Group();
		ScoreTab score = new ScoreTab(client);
		GameTab game = new GameTab(client);
		System.out.println(game.getAttempt());
		
		GridPane pane=new GridPane();
		Text q=score.getQueenBee();
		pane.add(q,0,0,1,1);
		Text g=score.getGenious();
		pane.add( g, 0,1, 1,1);
		Text a=score.getAmazing();
		pane.add(a, 0,2,1,1);
		Text gd=score.getGood();
		pane.add(gd, 0,3,1,1);
		Text gs=score.getStarted();
		pane.add(gs, 0,4,1,1);
		Text cs=score.getCurrentScore();
		pane.add(cs, 0,5,1,1);
		TextField qv=score.getQueenBeeVal();
		pane.add(qv, 1,0,1,1);
		TextField gv= score.getGeniousVal();
		pane.add(gv, 1,1,1,1);
		TextField av=score.getAmazingVal();
		pane.add(av, 1,2,1,1);
		TextField gdv=score.getGoodVal();
		pane.add(gdv, 1,3,1,1);
		TextField sv=score.getStartedVal();
		
		pane.add(sv, 1,4,1,1);
		TextField csv=score.getCurrentScoreVal();
		pane.add(csv, 1,5,1,1);
		TextField scorefield=score.getScoreField();
		pane.add(scorefield, 1,6,1,1);
		game.getScoreField().textProperty().addListener(new ChangeListener<String>() {
	         public void changed(ObservableValue<? extends String> observable, String old, String newV) 	{
	        	score.refresh();
	         }
	    });
		pane.setHgap(10);
		
		GridPane gamepane = new GridPane();
		GridPane gamepaneB = new GridPane();
		Button firstLetterB = game.getFirstLetterB();
		gamepaneB.add(firstLetterB, 0, 0, 1, 1);
		Button secondLetterB = game.getSecondLetterB();
		gamepaneB.add(secondLetterB, 1, 0, 1, 1);
		Button thirdLetterB = game.getThirdLetterB();
		gamepaneB.add(thirdLetterB, 2, 0, 1, 1);
		Button fourthCenterLetterB = game.getFourthCenterLetterB();
		gamepaneB.add(fourthCenterLetterB, 3, 0, 1, 1);
		Button fifthLetterB = game.getFifthLetterB();
		gamepaneB.add(fifthLetterB, 4, 0, 1, 1);
		Button sixthLetterB = game.getSixthLetterB();
		gamepaneB.add(sixthLetterB, 5, 0, 1, 1);
		Button seventhLetterB = game.getSeventhLetterB();
		gamepaneB.add(seventhLetterB, 6, 0, 1, 1);
		
		gamepane.add(gamepaneB, 0, 1);
		TextField attemptField = game.getAttemptField();
		attemptField.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String old, String newV) {
				System.out.println(old+" "+newV);
				game.setAttempt(newV);
			}
		});
		gamepane.add(attemptField, 0, 2, 7, 1);
		
		firstLetterB.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				attemptField.setText(attemptField.getText()+firstLetterB.getText());
			}
		});
		
		secondLetterB.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				attemptField.setText(attemptField.getText()+secondLetterB.getText());
			}
		});
		thirdLetterB.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				attemptField.setText(attemptField.getText()+thirdLetterB.getText());
			}
		});
		fourthCenterLetterB.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				attemptField.setText(attemptField.getText()+fourthCenterLetterB.getText());
			}
		});
		fifthLetterB.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				attemptField.setText(attemptField.getText()+fifthLetterB.getText());
			}
		});
		sixthLetterB.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				attemptField.setText(attemptField.getText()+sixthLetterB.getText());
			}
		});
		seventhLetterB.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				attemptField.setText(attemptField.getText()+seventhLetterB.getText());
			}
		});
		Button submitButton = game.getSubmitButton();
		gamepane.add(submitButton, 0, 3, 2, 1);
		
		TextField messageField = game.getMessageField();
		TextField scoreField = game.getScoreField();
		
		gamepane.add(messageField, 0, 4, 2, 1);
		gamepane.add(scoreField, 3, 4, 2, 1);
		
		gamepane.setVgap(2);
		gamepane.setHgap(2);
		
		
		submitButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				System.out.println("submit word");
				game.setAttempt(attemptField.getText());
				messageField.setText(game.getMessageField().getText());
				scoreField.setText(game.getScoreField().getText());
				messageField.setText(client.sendAndWaitMessage("message;"+attemptField.getText()));
				int points = Integer.parseInt(client.sendAndWaitMessage("getPointsForWords;"+attemptField.getText()));
				int currentScore = Integer.parseInt(client.sendAndWaitMessage("getScore"));
				int newScore = points + currentScore;
				scoreField.setText(Integer.toString(newScore));
			}
		});
		
		Button clearButton = game.getClearB();
		Button deleteButton = game.getDeleteB();
		gamepane.add(clearButton, 2, 3, 2, 1);
		gamepane.add(deleteButton, 4, 3, 3, 1);
		
		clearButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				System.out.println("clear word");
				attemptField.setText("");
			}
		});
		deleteButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				System.out.println("delete last character");
				String str = attemptField.getText();
				attemptField.setText(str.substring(0, str.length()-1));
			}
		});
		
		game.setContent(gamepane);
	
		
		score.setContent(pane);
		gameAndScore.getTabs().add(score);
		gameAndScore.getTabs().add(game);
		
	
		 VBox vbox=new VBox(gameAndScore) ;
		 root.getChildren().add(vbox);
		 
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.WHITE);
		//associate scene to stage and show
		stage.setTitle("SpellingBee Game"); 
		stage.setScene(scene); 
		stage.show();  

		}
	
	public static void main(String[] args) {
	        Application.launch(args);
	 }
	
	
}
